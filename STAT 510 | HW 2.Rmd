---
title: "STAT 510 | HW 2"
author: "Ricardo Batista"
date: "1/24/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Question 1

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; From the definition of generalized inverse we have

$$
X'X \left(X'X\right)^{-} X'X = X'X.
$$
Recall the result from HW 1 question 7, namely that

$$
X'XA = X'XB \iff XA = XB.
$$

Now, letting $A \equiv \left(X'X\right)^{-} X'X$ and $B \equiv I$, we have

$$
X'X \left(X'X\right)^{-} X'X = X'X I \iff X \left(X'X\right)^{-} X'X = X.
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; Since $G$ is a generalized inverse of $A$ we have that

$$
AGA = A.
$$
Transposing both sides

$$
\begin{aligned}
(AGA)' &= A'\\[10pt]
A'G'A' &= A'.
\end{aligned}
$$

Since $A$ is symmetric, i.e., $A = A'$, so

$$
AG'A = A,
$$

which is what we set to prove.  

\pagebreak

## (c)

&nbsp;&nbsp;&nbsp;&nbsp; Again from the definition of generalized inverse we have

$$
X'X \left(X'X\right)^{-} X'X = X'X,
$$

and transposing both sides

$$
\begin{aligned}
\left(X'X \left(X'X\right)^{-} X'X \right)' &= (X'X)'
\\[10pt]
X'X {\left(X'X\right)^{-}}' X'X &= X'X
\end{aligned}
$$

reveals that ${\left(X'X\right)^{-}}'$ is a generalized inverse of $X'X$. Then

$$
\begin{aligned}
X {\left(X'X\right)^{-}}' X'X &= X && \text{by part (a)}\\[10pt]
\left(X {\left(X'X\right)^{-}}' X'X\right)' &= X'\\[10pt]
X'X \left(X'X\right)^{-} X' &= X'.
\end{aligned}
$$

## (d)

&nbsp;&nbsp;&nbsp;&nbsp; Using part (a), we have that

$$
\begin{aligned}
X\left(X'X\right)^{-}X'X &= X
\\[10pt]
X\left(X'X\right)^{-}X'X \left(\left(X'X\right)^{-}X'\right) 
&= X\left(\left(X'X\right)^{-}X'\right)
&& 
\text{right-multiplying both sides by } \left(X'X\right)^{-}X'
\\[10pt]
X\left(X'X\right)^{-}X'\left(X \left(X'X\right)^{-}X'\right) 
&= X\left(X'X\right)^{-}X'
\\[10pt]
P_X P_X &= P_X
\end{aligned}
$$

&nbsp;&nbsp;&nbsp;&nbsp; Now, using part (c), we have that


$$
\begin{aligned}
X'X \left(X'X\right)^{-}X' &= X'
\\[10pt]
\left(X\left(X'X\right)^{-}\right)X'X \left(X'X\right)^{-}X' 
&= \left(X\left(X'X\right)^{-}\right)X'
&& 
\text{left-multiplying both sides by } X\left(X'X\right)^{-}
\\[10pt]
\left(X\left(X'X\right)^{-}X'\right)X \left(X'X\right)^{-}X' 
&= X\left(X'X\right)^{-}X'
\\[10pt]
P_X P_X &= P_X
\end{aligned}
$$

\pagebreak

## (e)

&nbsp;&nbsp;&nbsp;&nbsp; From part (c), take

$$
\begin{aligned}
X'X\left(X'X\right)^{-}X' &= X'
\\[10pt]
X'XG_1X' &= X'
\end{aligned}
$$

 and multiply it by the LHS and RHS of our result in part (a)

$$
\begin{aligned}
X'XG_1X'X &= X'X\left(X'X\right)^{-}X'X
\\[10pt]
X'XG_1X'X &= X'XG_2X'X
\\[10pt]
XG_1X'X &= XG_2X'X
&& X'XA = X'XB \implies XA = XB \hspace{10pt} \text{ (by HW 1 Q7)}
\\[10pt]
XG_1X'XG_1X' &= XG_2X'XG_1X'
&& \text{right-multiply both sides by } G_1X'
\\[10pt]
XG_1X' &= XG_2X'.
\end{aligned}
$$

## (f)

&nbsp;&nbsp;&nbsp;&nbsp; Note that $(X'X)' = X'X$, so $X'X$ is symmetric. Therefore, we have that for any generalized inverse $G_1$ of $X'X$, $G_1'$ is also a generalized inverse. Therefore

$$
\begin{aligned}
X'XG_1'X'X &= X'X
\\[10pt]
X'XG_1'X'X &= X'XG_1X'X
\end{aligned}
$$

and we proceed as in part (c)

$$
\begin{aligned}
X'XG_1'X'X &= X'XG_1X'X
\\[10pt]
XG_1'X'X &= XG_1X'X
&& X'XA = X'XB \implies XA = XB \hspace{10pt} \text{ (by HW 1 Q7)}
\\[10pt]
XG_1'X'XG_1X' &= XG_1X'XG_1X'
&& \text{right-multiply both sides by } G_1X'
\\[10pt]
XG_1'X' &= XG_1X'.
\end{aligned}
$$

\pagebreak

# Question 2

&nbsp;&nbsp;&nbsp;&nbsp; Note $P_Xy - z \neq 0$ and 

$$
\begin{aligned}
(y - P_Xy)'(P_Xy - z)
&= 
(y' - (P_Xy)')(P_Xy - z)
\\[10pt]
&=
(y' - y'P_X')(P_Xy - z)
\\[10pt]
&=
y'P_Xy - y'z - y'P_X'P_Xy + y'P_X'z
\\[10pt]
&=
y'P_Xy - y'z - y'P_Xy + y'P_Xz
&& P_X \text{ is sym. and idemp.}
\\[10pt]
&=
- y'z + y'z = 0
&& z \in \mathcal{C} \text{ and } P_X \text{is the identity operator } I \text{ on } U.
\end{aligned}
$$

Therefore, using the hint from the prompt with $a = y - P_Xy$ and $b = P_Xy - z$, 

$$
\begin{aligned}
\vert\vert y - z \vert\vert^2
&=
\vert\vert y - P_Xy + P_Xy - z \vert\vert^2
\\[10pt]
&> 
\vert\vert y - P_Xy \vert\vert^2
\end{aligned}
$$

and squaring both sides yields the result.


# Question 3

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{C}(P_X) \subset \mathcal{C}(X)}$  

&nbsp;&nbsp;&nbsp;&nbsp; Let $y \in \mathbb{R}^n$, so $P_Xy \in \mathcal{C}(P_X)$. Recall that $P_X$ is known as the orthogonal-projection matrix onto the column space of $X$ (slide 2 slide set 2). That is, 

$$
P_Xy \in \mathcal{C}(X).
$$

In any case, note that $P_Xy = X\left(X'X\right)^{-}X'y = Xz$ where $z \in \mathbb{R}^p$.

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\mathcal{C}(X) \subset \mathcal{C}(P_Xy)}$  

&nbsp;&nbsp;&nbsp;&nbsp; Let $b \in \mathbb{R}^p$, so $Xb \in \mathcal{C}(X)$. Now recall that $P_XX = X$, and, since $Xb \in \mathbb{R}^n$,

$$
P_XXb = Xb \in \mathcal{C}(P_Xy).
$$


# Question 4

&nbsp;&nbsp;&nbsp;&nbsp; Subbing in $\left(X'X\right)^{-}X'y$ for $b$ in the Normal equations

$$
\begin{aligned}
X'Xb &= X'y\\[10pt]
X'X\left(X'X\right)^{-}X'y &= X'y
\end{aligned}
$$

and using our results in "Question 1", namely that $X'X \left(X'X\right)^{-}X' = X'$, 

$$
\begin{aligned}
X'y &= X'y
\end{aligned}
$$

which reveals a consistent system of equations, i.e., the result we set to prove.  

# Question 5

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; First note that since $C\hat\beta$ is a linear transformation of multivariate normal vector $y$, the resulting distribution will also be multivariate normal. Now let $X$ be a $n \times p$ matrix and consider

$$
\mathbb{E}\left[C \hat\beta\right]
= 
AX\left(X'X\right)^{-}X'\mathbb{E}\left[y\right]
= 
AP_XX\beta
= 
AX\beta
=
C\beta.
$$

&nbsp;&nbsp;&nbsp;&nbsp; Now, consider

$$
\begin{aligned}
Var(C \hat\beta)
&=
Var\left(C\left(X'X\right)^{-}X'y\right)
=
C\left(X'X\right)^{-}X' Var(y) \left(C\left(X'X\right)^{-}X'\right)'
\\[10pt]
&=
C\left(X'X\right)^{-}X' \sigma^2I X \left(X'X\right)^{-'}C'
\\[10pt]
&=
\sigma^2AX\left(X'X\right)^{-} X'X \left(X'X\right)^{-'}X'A'
\\[10pt]
&=
\sigma^2AP_XP_XA'
= \sigma^2AP_XA'
= \sigma^2AX\left(X'X\right)^{-} X'A'
\\[10pt]
&=
\sigma^2C\left(X'X\right)^{-}C'
\end{aligned}
$$

where we used the fact that $X \left(X'X\right)^{-'}X' = P_X' = P_X$. Therefore, since our expression for $Var(C \hat\beta)$ is the variance of normally distributed r.v. $y$ times a constant matrix, the distribution in question is

$$
C \hat \beta \sim N \left(C\beta, \sigma^2 C \left(X'X\right)^{-}C'\right). 
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; Without further information about $X$ (e.g., whether it is full-rank), 

$$
\begin{aligned}
Var(C \hat\beta)
&=
Var\left(C\left(X'X\right)^{-}X'y\right)
=
\sigma^2C\left(X'X\right)^{-} X'X \left(X'X\right)^{-'}C'.
\end{aligned}
$$

\pagebreak

## (c)

&nbsp;&nbsp;&nbsp;&nbsp; Note that since the hypothesis is testable, $c'\beta$ is estimable. Then we have that

$$
\begin{aligned}
c' \hat \beta \sim N \left(c'\beta, \sigma^2 c' \left(X'X\right)^{-}c\right)
&&
\frac{(n - r)\hat{\sigma}^2}{\sigma^2} \sim \mathcal{X}_{n - r}^2
\end{aligned}
$$

and we can write

$$
\begin{aligned}
t
&=
\frac{c'\hat{\beta} - d}{\sqrt{\hat\sigma^2 c' \left(X'X\right)^{-}c}}
= 
\frac{c'\hat{\beta} - d}{\sqrt{\frac{\sigma^2(n-r)}{\sigma^2(n-r)}\hat\sigma^2 c' \left(X'X\right)^{-}c}}
=
\frac{
\frac{c'\hat{\beta} - d}
{\sqrt{\sigma^2\hat c' \left(X'X\right)^{-}c}}}
{\sqrt{\frac{\frac{(n-r)\hat\sigma^2}{\sigma^2}}{(n-r)}}}.
\end{aligned}
$$

Note that the numerator is still a normal distribution but with mean

$$
\frac{c'{\beta} - d}
{\sqrt{\sigma^2 c' \left(X'X\right)^{-}c}}
$$

and variance

$$
\begin{aligned}
Var\left(\frac{c'{\hat\beta} - d}{\sqrt{\sigma^2 c' \left(X'X\right)^{-}c}}\right)
=
\frac{Var\left(c'{\hat\beta}\right)}{\sigma^2 c' \left(X'X\right)^{-}c}
=
\frac{\sigma^2 c' \left(X'X\right)^{-}c}{\sigma^2 c' \left(X'X\right)^{-}c}
= 1.
\end{aligned}
$$

Therefore, given $c'\hat\beta$ and $\hat\sigma$ are independent (slide 19 in set 2), 

$$
t = 
\frac{c'\hat{\beta} - d}{\sqrt{\hat\sigma^2 c' \left(X'X\right)^{-}c}}
=
\frac{
\frac{c'\hat{\beta} - d}
{\sqrt{\sigma^2 c' \left(X'X\right)^{-}c}}}
{\sqrt{\frac{\frac{(n-r)\hat\sigma^2}{\sigma^2}}{(n-r)}}}
\sim t_{(n - r)}\left(\frac{c'{\beta} - d}
{\sqrt{\sigma^2 c' \left(X'X\right)^{-}c}}\right).
$$

by slide 39 of slide set 1.  
